package com.societe.DAO;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.societe.model.Applicant;
import com.societe.model.ApplicantSkill;

@Repository
public interface ApplicantDAO extends JpaRepository<Applicant, Integer>{

	List<Applicant> findByExperience(int experince);

	@Query("SELECT a FROM Applicant a  JOIN  a.skillSet s WHERE s.skillType IN  (:ApplicantSkill)")
	List<Applicant> findBySkillSet(@Param("ApplicantSkill") String[] skills);
//	@Query("SELECT a FROM Applicant a WHERE a.email = :email")
	Applicant findByEmail(String email);

	List<Applicant> findBySkillSet(Set<ApplicantSkill> skills);
	
	Applicant findByApplicantId(int applicantId);
}
