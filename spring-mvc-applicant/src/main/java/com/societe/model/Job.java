package com.societe.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.societe.model.Skills;

//@Entity
//@Table(name = "PROJ_JOB")
public class Job {
	private String title;
//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "jobseq")
//	@SequenceGenerator(name="jobseq",sequenceName = "job_seq")
	private Integer jobId;
	private String profile;
	private String dept;
	private Integer experience;
	private String type;
	
	private String location;
//	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE }
//
//			)
//			@JoinTable(name = "job_skill", joinColumns = { @JoinColumn(name = "JOB_ID") }, inverseJoinColumns = {
//					@JoinColumn(name = "SKILLS_ID") })
//			@JsonIgnore
	private Set<Skills> skillSet= new HashSet<>();
	private Integer vacancy;
	
	public Integer getVacancy() {
		return vacancy;
	}
	public void setVacancy(Integer vacancy) {
		this.vacancy = vacancy;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getJobId() {
		return jobId;
	}
	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public Integer getExperience() {
		return experience;
	}
	public void setExperience(Integer experience) {
		this.experience = experience;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	

	public Job(String title, Integer jobId, String profile, String dept, Integer experience, String type,
			String location, Set<Skills> skillSet, Integer vacancy) {
		super();
		this.title = title;
		this.jobId = jobId;
		this.profile = profile;
		this.dept = dept;
		this.experience = experience;
		this.type = type;
		this.location = location;
		this.skillSet = skillSet;
		this.vacancy = vacancy;
	}
	@Override
	public String toString() {
		return "Job [title=" + title + ", jobId=" + jobId + ", profile=" + profile + ", dept=" + dept + ", experience="
				+ experience + ", type=" + type + ", location=" + location + ", skillSet=" + skillSet + ", vacancy="
				+ vacancy + "]";
	}
	public Job() {
		super();
	}
	public Set<Skills> getSkillSet() {
		return skillSet;
	}
	public void setSkillSet(Set<Skills> skillSet) {
		this.skillSet = skillSet;
	}
	
	
}
