package com.societe.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.societe.DAO.ApplicantDAO;
import com.societe.DAO.ApplicationDAO;
import com.societe.model.Applicant;
import com.societe.model.ApplicantSkill;
import com.societe.model.Application;
import com.societe.model.Job;
import com.sun.mail.smtp.SMTPTransport;

@Service
public class ApplicantServiceImpl implements ApplicantService {

	@Autowired
	ApplicantDAO applicantDAO;

	@Autowired
	ApplicationDAO applicationDAO;

	@Autowired
	RestTemplate restTemplate;

	// this connects to client service which connects to job service

	@Override
	public List<Applicant> getAllApplicants() {
		return applicantDAO.findAll();
	}

	@Override
	public Applicant getApplicantById(int applicantID) {
		return applicantDAO.getOne(applicantID);
	}

	@Override
	public List<Applicant> getApplicantBySkill(String[] skills) {
		return applicantDAO.findBySkillSet(skills);
	}

	@Override
	public List<Applicant> getAppicantByExperience(int experience) {
		return applicantDAO.findByExperience(experience);
	}

	@Override
	public Applicant getAppicantByEmail(String email) {
		return applicantDAO.findByEmail(email);
	}

	@Override
	public void help() {

	}

	@Override
	public Application applyForJob(Job job, int applicantID) {
		Application application = new Application();
		application.setJobID(job.getJobId());
		application.setApplicant(applicantDAO.getOne(applicantID));
		applicationDAO.save(application);
		return application;
	}

	@Override
	public Set<Job> recommendations(Applicant applicant) {
		return null;
	}

	@Override
	public void bookmarkForLater(Job job) {

	}

	/*
	 * @Override public List<Application> trackStatus(Applicant applicant) {
	 * List<Application> applicationList =
	 * applicationDAO.findByApplicant(applicant); return applicationList; }
	 */

	@Override
	public boolean signUp(Applicant applicant) {
		applicantDAO.save(applicant);
		return true;
	}

	@Override
	public boolean logIn(String email, String password) {
		Applicant applicant = applicantDAO.findByEmail(email);
		if (applicant == null)
			return false;
		if (applicant.getPassword().equals(password))
			return true;
		else
			return false; 

	}

	/**
	 * @param recipientEmail
	 * @param interviewDatesLink send mail to applicant about interview dates which
	 *                           are decided by HR
	 * @author Veeroji Kale
	 */

	@Override
	public void sendAvaiableInterviewDatesLink(String recipientEmail, String interviewDatesLink) {

		final String SMTP_SERVER = "smtp.gmail.com"; // for out going email
		final String USERNAME = "veerojikale@gmail.com";
		final String PASSWORD = "veerojikale709";

		final String EMAIL_FROM = "veerojikale@gmail.com";
		final String EMAIL_TO = recipientEmail; // applicant email
		final String EMAIL_TO_CC = "safnahassan16@gmail.com";

		final String EMAIL_SUBJECT = "Available Interview Dates from HR";
		final String EMAIL_TEXT = interviewDatesLink; // body of the mail(link)

		Properties prop = System.getProperties();
		prop.put("mail.smtp.host", SMTP_SERVER); // optional, defined in SMTPTransport
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.port", "25"); // default port 25

		Session session = Session.getInstance(prop, null);
		Message msg = new MimeMessage(session);

		try {

			// from
			msg.setFrom(new InternetAddress(EMAIL_FROM));

			// to
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(EMAIL_TO, false));

			// cc
			msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(EMAIL_TO_CC, false));

			// subject
			msg.setSubject(EMAIL_SUBJECT);

			// content
			msg.setText(EMAIL_TEXT);

			msg.setSentDate(new Date());

			// Get SMTPTransport
			SMTPTransport t = (SMTPTransport) session.getTransport("smtp");

			// connect
			t.connect(SMTP_SERVER, USERNAME, PASSWORD);

			// send
			t.sendMessage(msg, msg.getAllRecipients());

			System.out.println("Response: " + t.getLastServerResponse());

			t.close();

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

}
