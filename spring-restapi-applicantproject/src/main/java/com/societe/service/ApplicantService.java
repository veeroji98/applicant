package com.societe.service;

import java.util.List;
import java.util.Set;

import com.societe.model.Applicant;
import com.societe.model.ApplicantSkill;
import com.societe.model.Application;
import com.societe.model.Job;

public interface ApplicantService {
	
	List<Applicant> getAllApplicants();
	Applicant getApplicantById(int applicantID);
	List<Applicant> getApplicantBySkill(String[] skills);
	List<Applicant> getAppicantByExperience(int experience);
	Applicant getAppicantByEmail(String email);
	
	//for applicant signup
	public boolean signUp(Applicant applicant);
//	
//	//for applicant login
	public boolean logIn(String email, String password);

	//filling application form and uploading 
	public Application applyForJob(Job job, int applicantID);
	
	//we have to go to a different view which shows application process
	//give notification from here too
	//from HR module
	
	//public List<Application> trackStatus(Applicant applicant);
	
	//from Job module
	//To give recommendations based on skillset
	public Set<Job> recommendations(Applicant applicant);
	
	public void bookmarkForLater(Job job);
	
	public void help();
	
	/**
	 * sending available interview dates link to the applicant through applicant email id
	 */
	public void sendAvaiableInterviewDatesLink(String email, String link);
	
	
	
}
