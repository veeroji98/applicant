package com.societe.service;

import java.util.List;

import com.societe.model.Application;


public interface ApplicationService {
	
	List<Application> getAllApplications();
	

}
