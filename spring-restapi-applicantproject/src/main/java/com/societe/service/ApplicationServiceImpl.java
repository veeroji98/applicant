package com.societe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.societe.DAO.ApplicationDAO;
import com.societe.model.Application;

@Service
public class ApplicationServiceImpl implements ApplicationService{
	
	@Autowired
	ApplicationDAO applicationDAO;

	@Override
	public List<Application> getAllApplications() {
		return applicationDAO.findAll();
	}

}
