package com.societe.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.societe.model.Applicant;
import com.societe.model.Application;

@Repository
public interface ApplicationDAO extends JpaRepository<Application, Integer> {
	List<Application> findByApplicant(Applicant applicant);

}
