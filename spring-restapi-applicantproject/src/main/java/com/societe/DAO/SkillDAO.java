package com.societe.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.societe.model.Application;

@Repository
public interface SkillDAO extends JpaRepository<Application, Integer> {

}
