package com.societe.exception;

public class NoApplicantFoundException extends Exception {

	public NoApplicantFoundException() {
		super();
	}

	public NoApplicantFoundException(String message) {
		super(message);
	}

}
