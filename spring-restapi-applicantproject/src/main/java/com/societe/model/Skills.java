package com.societe.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PROJ_SKILL")
public class Skills {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "skillseq")
	@SequenceGenerator(name = "skillseq", sequenceName = "Skill_Seq")
	private int skillId;

	private String skillType;
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "skillSet", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private Set<Job> joblist = new HashSet<>();

	public Skills() {
		super();
	}

	public Set<Job> getJoblist() {
		return joblist;
	}

	public void setJoblist(Set<Job> joblist) {
		this.joblist = joblist;
	}

	public int getSkillId() {
		return skillId;
	}

	public void setSkillId(int skillId) {
		this.skillId = skillId;
	}

	public String getSkillType() {
		return skillType;
	}

	public void setSkillType(String skillType) {
		this.skillType = skillType;
	}

	@Override
	public String toString() {
		return "Skills [skillId=" + skillId + ", skillType=" + skillType + "]";
	}

	public Skills(int skillId, String skillType) {
		super();
		this.skillId = skillId;
		this.skillType = skillType;
	}

}
