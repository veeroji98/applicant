 package com.societe.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class Applicant {
	private String name; 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "applicantseq")
	@SequenceGenerator(name="applicantseq", sequenceName = "APPLICANT_SEQ")
	private int applicantId;
	private String email;
	private String password;
	@ManyToMany(fetch = FetchType.LAZY,
			cascade = { CascadeType.PERSIST,CascadeType.MERGE })
	@JoinTable(name = "applicants_skills",
				joinColumns = {@JoinColumn(name="APPLICANT_ID")},
				inverseJoinColumns = {@JoinColumn(name="SKILL_ID")}
			)
	@JsonIgnore
	private Set<ApplicantSkill> skillSet = new HashSet<>();
	private Date dob;
	private String qualifications;
	//ft, pt or intern
	private String type;
	private int experience;
	private String linkedInProfile;
	
	
	
	public int getApplicantId() {
		return applicantId;
	}
	public void setApplicantId(int applicantId) {
		this.applicantId = applicantId;
	}
	public Set<ApplicantSkill> getSkillSet() {
		return skillSet;
	}
	public void setSkillSet(Set<ApplicantSkill> skillSet) {
		this.skillSet = skillSet;
	}
	public String getLinkedInProfile() {
		return linkedInProfile;
	}
	public void setLinkedInProfile(String linkedInProfile) {
		this.linkedInProfile = linkedInProfile;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getApplicantID() {
		return applicantId;
	}
	public void setApplicantID(int applicantID) {
		this.applicantId = applicantID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Set<ApplicantSkill> getSkillset() {
		return skillSet;
	}
	public void setSkillset(Set<ApplicantSkill> skillset) {
		this.skillSet = skillset;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getQualifications() {
		return qualifications;
	}
	public void setQualifications(String qualifications) {
		this.qualifications = qualifications;
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
	@Override
	public String toString() {
		return "Applicant [name=" + name + ", applicantId=" + applicantId + ", email=" + email + ", password="
				+ password + ", skillSet=" + skillSet + ", dob=" + dob + ", qualifications=" + qualifications
				+ ", type=" + type + ", experience=" + experience + ", linkedInProfile=" + linkedInProfile + "]";
	}
	
}
