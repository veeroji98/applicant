package com.societe.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class Application {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "applicationseq")
	@SequenceGenerator(name="applicationseq", sequenceName = "APPLICATION_SEQ")
	private int applicationId;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "applicant_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
	private Applicant applicant;
	private Integer jobID;
	private boolean resumeShortList;
	private boolean techInterview;
	private boolean hrInterview;
	private boolean appointed;
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public Applicant getApplicant() {
		return applicant;
	}
	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}
	public Integer getJobID() {
		return jobID;
	}
	public void setJobID(Integer jobID) {
		this.jobID = jobID;
	}
	public boolean isResumeShortList() {
		return resumeShortList;
	}
	public void setResumeShortList(boolean resumeShortList) {
		this.resumeShortList = resumeShortList;
	}
	public boolean isTechInterview() {
		return techInterview;
	}
	public void setTechInterview(boolean techInterview) {
		this.techInterview = techInterview;
	}
	public boolean isHrInterview() {
		return hrInterview;
	}
	public void setHrInterview(boolean hrInterview) {
		this.hrInterview = hrInterview;
	}
	public boolean isAppointed() {
		return appointed;
	}
	public void setAppointed(boolean appointed) {
		this.appointed = appointed;
	}
	@Override
	public String toString() {
		return "Application [applicationId=" + applicationId + ", applicant=" + applicant + ", jobID=" + jobID
				+ ", resumeShortList=" + resumeShortList + ", techInterview=" + techInterview + ", hrInterview="
				+ hrInterview + ", appointed=" + appointed + "]";
	}
	
	
	
}
