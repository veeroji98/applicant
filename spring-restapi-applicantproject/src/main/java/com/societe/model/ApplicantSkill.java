package com.societe.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class ApplicantSkill {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "skillseq")
	@SequenceGenerator(name = "skillseq", sequenceName = "SKILL_SEQ")
	@Column(name = "skillid")
	private Integer skillId;
	@Column(name = "skilltype")
	private String skillType;
	
	@ManyToMany(fetch = FetchType.LAZY,
			cascade = { CascadeType.PERSIST,CascadeType.MERGE },
			mappedBy = "skillSet")
	private Set<Applicant> applicantList = new HashSet<>();

	public Integer getSkillId() {
		return skillId;
	}

	public void setSkillId(Integer skillId) {
		this.skillId = skillId;
	}

	public String getSkillType() {
		return skillType;
	}

	public void setSkillType(String skillType) {
		this.skillType = skillType;
	}

	public Set<Applicant> getApplicantList() {
		return applicantList;
	}

	public void setApplicantList(Set<Applicant> applicantList) {
		this.applicantList = applicantList;
	}

	@Override
	public String toString() {
		return "Skill [skillId=" + skillId + ", skillType=" + skillType + ", applicantList=" + applicantList + "]";
	}

}
