package com.societe.controller;

import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.discovery.EurekaClient;
import com.societe.DAO.ApplicantDAO;
import com.societe.model.Applicant;
import com.societe.model.ApplicantSkill;
import com.societe.model.Application;
import com.societe.model.Job;
import com.societe.service.ApplicantService;
import com.societe.service.ApplicationService;

@RestController
@RequestMapping("applicant-service")
public class ApplicantController {
	@Autowired
	ApplicantService applicantService;
	@Autowired
	ApplicationService applicationService;
	
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	EurekaClient eurekaClient;
	 @Autowired
    private JavaMailSender sender;
    
    @Autowired
    private ApplicantDAO applicantDAO;
    
  //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "F://temp//";
	
	public final String BASEURL = "http://localhost:8020/freelance-client-service";
	
	@GetMapping("/applicants/signup")
	public boolean signUp(@ModelAttribute("applicant") Applicant applicant) {
		if(applicantService.getAppicantByEmail(applicant.getEmail()) != null) {
			return false;
		}
		return applicantService.signUp(applicant);
	}
	
	@PostMapping("/applicants/login")
	public boolean logIn(@RequestParam("email") String email, @RequestParam("password") String password)  {
		return applicantService.logIn(email, password);
	}
	
	@GetMapping("/applicants")
	public List<Applicant> getAllApplicants() {
		return applicantService.getAllApplicants();
	}
	
	@GetMapping("/applicants/{applicantId}")
	public Applicant getApplicantById(@RequestParam("applicantId") Integer applicantId) {
		return applicantService.getApplicantById(applicantId);
	}
	
	@GetMapping("/applicants/{skills}")
	public List<Applicant> getApplicantBySkill(@RequestParam("skills") String[] skills){
		return applicantService.getApplicantBySkill(skills);
	}
	
	@GetMapping("/applicants/{experience}")
	public List<Applicant> getAppicantByExperience(@RequestParam("experience") Integer experience){
		return applicantService.getAppicantByExperience(experience);
	}
	
	@GetMapping("/applicants/job/{job}/{applicantId}")
	public Application applyForJob(@RequestParam("job") Job job,@RequestParam("applicantId") Integer applicantId) {
		return applicantService.applyForJob(job, applicantId);
	}
	
	@GetMapping("/applicants/recommendations/{applicant}")
	public Set<Job> recommendations(Applicant applicant){
		return applicantService.recommendations(applicant);
	}
	
	
    @RequestMapping("/sendMail/{applicantId}/{subject}/{body}")
    public String sendMail(@PathVariable("applicantId") int applicantId,
    					   @PathVariable("subject") String subject,
    					   @PathVariable("message") String body) {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
            helper.setTo(applicantDAO.finByApplicationId(applicantId).getEmail());
            helper.setText(body);
            helper.setSubject(subject);
        } catch (MessagingException e) {
            e.printStackTrace();
            return "Error while sending mail ..";
        }
        sender.send(message);
        return "Mail Sent Success!";
    }

    @GetMapping("/applications")
    public List<Application> getAllApplications() {
		return applicationService.getAllApplications();
	}

} 