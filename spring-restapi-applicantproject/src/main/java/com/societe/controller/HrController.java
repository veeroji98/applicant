package com.societe.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.societe.model.Applicant;
import com.societe.model.Application;
import com.societe.service.ApplicantService;
import com.societe.service.ApplicationService;

@RestController
public class HrController {
	
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	ApplicationService applicationService;
	@Autowired
	ApplicantService applicantService;
	
	
	private final String BASEURL = "http://localhost:8081/hr-service";
	private static final Logger log = LoggerFactory.getLogger(HrController.class);
	
	@GetMapping("/application/all")
	public List<Application> getAllApplications() {
		return applicationService.getAllApplications();
		
	}
	
	/**
	 * getting status of the applicant like 
	 * resume short listed
	 * techInterview 1
	 * Tech interview 2
	 * Hr Interview
	 * appointed
	 * @param applicantId
	 * @return applicant
	 */
	
	@GetMapping("/application/trackStatus/{applicantId}")
	public Application trackApplicantStatus(@RequestParam("applicantId") Integer applicantId) {
		String url = BASEURL + "/application/trackStatus/" + applicantId;
		return restTemplate.getForObject(url, Application.class);
	}
	
	
	//getting available dates from HR and send it to the applicant
	@GetMapping("/Dates")
	public void sendDates(){
		String url = BASEURL + "/sendDates";
		List<String> dates = restTemplate.getForObject(url, List.class);
		//dates = ["1425162712","hi your..."];
		//applicantService.sendEmail();
		
			Integer applicantId = Integer.parseInt(dates.get(0));
			String link = dates.get(1);
			Applicant applicant = applicantService.getApplicantById(applicantId);
			String email = applicant.getEmail();
			applicantService.sendAvaiableInterviewDatesLink(email, link);
			
		}
		
	}
	
