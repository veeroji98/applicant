
package com.societe.controller;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.societe.model.Applicant;
import com.societe.model.ApplicantSkill;
import com.societe.model.Job;
import com.societe.model.Skills;

@RestController
public class ClientController {
	@Autowired
	private RestTemplate restTemplate;
	
	private final String BASEURL = "http://localhost:8081//job-portal";
	private static final Logger log = LoggerFactory.getLogger(ClientController.class);
	
	@GetMapping("/applicants/job")
	public List<Job> getAllJobs() {
		String url = BASEURL + "/jobs";
		return restTemplate.getForObject(url, List.class);
		
	}
	
	@GetMapping("/applicants/job/type/{type}")
	public List<Job> showByType(@PathVariable String type) {
		String url = BASEURL + "/job/showJobsByType/" + type;
		return restTemplate.getForObject(url, List.class);
	} 
	
	@GetMapping("/applicants/job/skills/{applicant}")
	public List<Job> showBySkillSet(@RequestParam("applicant") Applicant applicant) {
		Set<ApplicantSkill> applicantSkillSet = applicant.getSkillset();
		
		Set<String> skills=null;
		for(ApplicantSkill s: applicantSkillSet) {
			skills.add(s.getSkillType());
		}
		
		int n = skills.size(); 
        String[] skillSet = skills.stream().toArray(String[] ::new);
        
        
		String url = BASEURL + "/job/showBySkillSet/" + skillSet;
		return restTemplate.getForObject(url, List.class);
	}
	
	@GetMapping("/applicants/job/location/{location}")
	public Set<Job> browseForJobByLocation(@PathVariable("location") String location) {
		String url = BASEURL + "/job/showJobsByLocation/" + location;
		return restTemplate.getForObject(url, Set.class);
	}
	
	@GetMapping("/applicants/job/department/{department}")
	public Set<Job> browseForJobByDept(@PathVariable("department") String department) {
		String url = BASEURL + "/job/showJobsByDept/" + department;
		return restTemplate.getForObject(url, Set.class);
	}
	
	@GetMapping("/applicants/job/profile/{profile}")
	public Set<Job> browseForJobByProfile(@PathVariable("profile") String profile) {
		String url = BASEURL + "/job/showJobsByProfile/" + profile;
		return restTemplate.getForObject(url, Set.class);
	}
	
	
	@GetMapping("/applicants/job/jobId/{jobId}")
	public Set<Job> browseForJobByJobId(@PathVariable("jobId") String jobId) {
		String url = BASEURL + "/job/showJobsByJobId/" + jobId;
		return restTemplate.getForObject(url, Set.class);
	}


}
